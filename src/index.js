import React from "react";
import ReactDOM from "react-dom";
import { ApolloClient } from "apollo-client";
import { HttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloProvider } from "react-apollo";
import App from "./App";
import "./index.css";

// it would be even better to store this URL in an environment variable
const GRAPH_CMS_API =
  "https://api-euwest.graphcms.com/v1/cjo72uny758zv01fuf0ub4zvk/master";

const client = new ApolloClient({
  link: new HttpLink({ uri: GRAPH_CMS_API }),
  cache: new InMemoryCache()
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById("root")
);
