import React from "react";
import { graphql } from "react-apollo";
import gql from "graphql-tag";

const SinglePost = ({ data: { loading, post } }) => {
  if (!loading) {
    return (
      <article className="blogpost">
        <div className="blogpost__post">
          <h1>{post.headline}</h1>
          <img src={post.image.url} alt="Alt Text" />
          <p dangerouslySetInnerHTML={{ __html: post.description }} />
        </div>
      </article>
    );
  }
  return <h2>Lade den Beitrag...</h2>;
};
const singlePost = gql`
  query singlePost($slug: String!) {
    post(where: { slug: $slug }) {
      id
      slug
      headline
      description
      image {
        url
      }
    }
  }
`;
export default graphql(singlePost, {
  options: ({ match }) => ({
    variables: {
      slug: match.params.slug
    }
  })
})(SinglePost);
