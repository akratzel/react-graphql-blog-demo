import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Header from "./Header";
import Blog from "./Blog";
import SinglePost from "./SinglePost";

const App = () => (
  <BrowserRouter>
    <main>
      <Header />
      <Switch>
        <Route exact path="/" component={Blog} />
        <Route path="/post/:slug" component={SinglePost} />
      </Switch>
    </main>
  </BrowserRouter>
);
export default App;
