import React from "react";
import { Link } from "react-router-dom";
import { graphql } from "react-apollo";
import gql from "graphql-tag";

const Blog = ({ data: { loading, posts } }) => {
  if (!loading) {
    return (
      <div className="blog">
        {posts.map(post => (
          <article className="blog__content" key={post.id}>
            <img src={post.image.url} alt="Alt Title" />
            <h2>{post.headline}</h2>
            <Link to={`/post/${post.slug}`}>
              <button className="blog__button">Mehr lesen</button>
            </Link>
          </article>
        ))}
      </div>
    );
  }
  return <h2>Lade alle Beiträge...</h2>;
};
const allPosts = gql`
  query posts {
    posts {
      headline
      description
      slug
      image {
        url
      }
    }
  }
`;
export default graphql(allPosts)(Blog);
