# Blog based on React and GraphQL

This little repo provides a demo application for a blog based on GraphQL and React.

Simply run `yarn && yarn start` and enjoy or visit the demo page: [http://quirky-foot.surge.sh/](http://quirky-foot.surge.sh/).
